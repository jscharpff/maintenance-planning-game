The repository has moved to the [TU Delft Algorithmics group repository](https://github.com/AlgTUDelft/road-maintenance-game) on GitHub.

The original article  
["Can multiple contractors self-regulate their joint service delivery? A serious gaming experiment on road maintenance planning"](https://www.tandfonline.com/doi/full/10.1080/01446193.2020.1806336) is published online in the Journal of Construction Management and Economics.